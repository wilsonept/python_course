#!/bin/python

def func(a, b):
    print("a: " + str(a))
    print("b: " + str(b))
    a = a + b
    b = a - b
    a = a - b
    print("a: " + str(a))
    print("b: " + str(b))

func(3, 10)
