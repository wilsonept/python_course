#!/usr/bin/python

from turtle import Turtle
from random import randrange

class Food(Turtle):
    def __init__(self):
        super().__init__()
        self.shape('circle')
        self.color('orange')
        self.penup()
        self.shapesize(stretch_len=0.7, stretch_wid=0.7)
        self.change_pos()

    def detect_snake(self, snake, scoreboard):
        if self.distance(snake.head) < 15:
            self.change_pos()
            snake.grow()
            scoreboard.add_score()
            
    def change_pos(self):
        xcor = randrange(-280, 280, 20)
        ycor = randrange(-280, 280, 20)
        self.goto(xcor, ycor)