#!/bin/python

from data import question_data
from quiz_brain import QuizBrain
from question import Question

question_bank = []
for item in question_data:
    question = Question(item['question'],item['correct_answer'])
    question_bank.append(question)

quiz = QuizBrain(question_bank)
quiz.next_question()
