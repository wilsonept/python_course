#!/bin/python

class QuizBrain():
    def __init__(self, question_bank):
        self.score = 0
        self.question_number = 0
        self.question_bank = question_bank

    def is_correct(self, user_answer, correct_answer):
        if user_answer.lower() == correct_answer.lower():
            self.score += 1
            print(f"That's right! Your score: {self.score}")
            return True
        else:
            print(f"Wrong answer, right answer was {correct_answer}")
            return False

    def still_has_questions(self):
        return len(self.question_bank) > self.question_number

    def next_question(self):
        current_question = self.question_bank[self.question_number]
        self.question_number += 1
        user_answer = input(f'Q.{self.question_number} : {current_question.text} : true/false : ')
        if self.is_correct(user_answer, current_question.answer):
            if self.still_has_questions():
                self.next_question()
            else:
                print(f'Game Over, your score: {self.score}')
