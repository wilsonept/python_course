#!/bin/python
question_data = [
    {
        "category":"Geography",
        "type":"boolean",
        "difficulty":"easy",
        "question":"The Republic of Malta is the smallest microstate worldwide.",
        "correct_answer":"False",
        "incorrect_answers":["True"]
    },
    {
        "category":"Entertainment: Video Games",
        "type":"boolean",
        "difficulty":"medium",
        "question":"The Snipers SMG in Team Fortress 2, was originally intended to be the Scouts primary weapon.",
        "correct_answer":"True",
        "incorrect_answers":["False"]
    },
    {
        "category":"Entertainment: Television",
        "type":"boolean",
        "difficulty":"medium",
        "question":"In the TV series Red Dwarf, Krytens full name is Kryten 2X4B-523P.",
        "correct_answer":"True",
        "incorrect_answers":["False"]
    },
    {
        "category":"General Knowledge",
        "type":"boolean",
        "difficulty":"easy",
        "question":"Adolf Hitler was born in Australia. ",
        "correct_answer":"False",
        "incorrect_answers":["True"]
    },
    {
        "category":"Politics",
        "type":"boolean",
        "difficulty":"easy",
        "question":"Donald Trump won the popular vote in the 2016 United States presidential election.",
        "correct_answer":"False",
        "incorrect_answers":["True"]
    },
    {
        "category":"Politics",
        "type":"boolean",
        "difficulty":"medium",
        "question":"Helen Clark was the 37th Prime Minister of Australia.",
        "correct_answer":"False",
        "incorrect_answers":["True"]
    },
    {
        "category":"Entertainment: Video Games",
        "type":"boolean",
        "difficulty":"medium",
        "question":"The character that would eventually become Dr. Eggman was considered for the role of Segas new flagship mascot before Sonic was.",
        "correct_answer":"True",
        "incorrect_answers":["False"]
    },
    {
        "category":"Entertainment: Music",
        "type":"boolean",
        "difficulty":"easy",
        "question":"Michael Jackson wrote The Simpsons song &quot;Do the Bartman&quot;.",
        "correct_answer":"False",
        "incorrect_answers":["True"]
    },
    {
        "category":"Entertainment: Japanese Anime & Manga",
        "type":"boolean",
        "difficulty":"hard",
        "question":"In the &quot;Kagerou Daze&quot; series, Shintaro Kisaragi is prominently shown with the color red.",
        "correct_answer":"True",
        "incorrect_answers":["False"]
    },
    {
        "category":"Entertainment: Music",
        "type":"boolean",
        "difficulty":"hard",
        "question":"The singer Billie Holiday was also known as &quot;Lady Day&quot;.",
        "correct_answer":"True",
        "incorrect_answers":["False"]
    }
]
