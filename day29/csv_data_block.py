# ------------------------------------- WORKING WITH DATA --------------------------------------------
def get_inputdata(field_obj):
    input_data = field_obj.get()
    return input_data.strip()

def check_entry(field_obj, search_string=''):
    '''return matched string or False'''
    input_data = get_inputdata(field_obj)
    if search_string == '':
        with open(DB) as raw_data:
            data = raw_data.readlines()
            for item in data:
                match = re.match(input_data, item)
                if match != None:
                    return item
    else:
        search_list = search_string.split(' | ')
        match = re.match(input_data, search_list[1])
        if match != None:
            return search_string
    return False

def check_db():
    match = check_entry(field_web)
    if match != False:
        match = check_entry(field_username, match)
        if match != False:
            answer = messagebox.askyesno(title='Duplicated entry', message='You already have entry with provided website and username, do you want to update password?')
            if answer:
                return ('rt', match.strip())
            else:
                return None
    return ('a')

def check_inputdata(website, username, password):
    if website != '' and username != '' and password != '':
        return True
    else:
        print('fill every single field')
        return False

def add_data():
    website = get_inputdata(field_web)
    username = get_inputdata(field_username)
    password = get_inputdata(field_pass)

    if check_inputdata(website, username, password) == True:
        mode = check_db()
        if mode[0] == 'a':
            with open(DB, mode=mode) as data:
                data.write(f'\n{website} | {username} | {password}')
        elif mode[0] == 'rt':
            print(mode[0])
            print(mode[1])
            with open(DB, mode=mode[0]) as file:
                data = file.read()
            data = data.replace(f'{mode[1]}', f'{website} | {username} | {password}')
            print(data)
            with open(DB, mode='w') as file:
                file.write(data)
    else:
        messagebox.showinfo(title='error', message="hey you, enter every single field")