#!/bin/python


print("Welcome to the Love Calculator!")
name1 = input("What is your name? \n")
name2 = input("What is their name? \n")

#name1 = 'dmitry zakharchenko'
#name2 = 'margarita kuleshova'

names = name1,name2
trlv = 'TRUE','LOVE'
trlv_c = []
counter = 0

for name in names:
    name = name.replace(' ','').upper()

    for word in trlv:
        for letter in word:
            counter += name.count(letter)

        trlv_c.append(counter)
        counter = 0

num1 = trlv_c[0] + trlv_c[2]
num2 = trlv_c[1] + trlv_c[3]

result = int(f'{num1}{num2}')

if result < 10 or result > 90:
    print(f'Your score is {result}, you go together like coke and mentos.')
elif result > 40 and result < 50:
    print(f'Your score is {result}, you are alright together.')
else:
    print(f'Your score is {result}.')

