#!/bin/python

from random import randint
from art import art
from words import words_list
from os import system as screen

def screen_update(art,lives,blank):
    print(art[lives])
    print(blank)


words_count = len(words_list)
word = words_list[randint(0, words_count - 1)]
blank = []

for _ in word:
    blank.append("_")

print(blank)


lives = 0
game_is_over = False
while not game_is_over:
    guess = input('Guess a letter: ').lower()

    screen('clear')

    for pos in range(len(word)):
        if word[pos] == guess:
            blank[pos] = guess

    if '_' not in blank:
        print('You win')
        break
    elif guess not in word:
        lives += 1
        screen_update(art,lives,blank)
    else:
        screen_update(art,lives,blank)

    if lives == 6:
        print('You lose')
        break
