#!/bin/python

import random
import os
import cards
from cards import cards, cards_deck, logo

def debug_print(message):
    print('[ DEBUG ] ' + str(message))

def get_card(step, cards_deck, player_cards):
    player_cards.append(cards_deck[step])
    return player_cards

def get_result(player_cards, cards):
    result = 0
    for card in player_cards:
        result += cards[card]

    if result > 21:
        if 'Ace' in player_cards:
            result -= 10

    return result

def print_winner(message):
    print('----------------------------------------------')
    print(message)

def print_stats():
    os.system('clear')
    print(logo)
    print(f'Dealer cards: {dealer_cards}')
    print(f'Dealer result: {dealer_real_result}')
    print(f'Player cards: {player_cards}')
    print(f'Player result: {player_result}')
    print(f'Your bank is {bank}$')

bank = 1000
actions_list = ['hit','stand','double']

while bank > 0:
    dealer_cards = []
    player_cards = []
    # перемешиваем колоду
    random.shuffle(cards_deck)

    os.system('clear')
    print(logo)
    print(f'Your bank is {bank}$')
    print('-------------------------------------------------')
    bet = int(input('Make your bet: 100, 200 or 500: '))

    # задаем смещение колоде, что бы не испльзовать уже выданные карты
    deck_step = 4
    queue_counter = 0
    # сдаем карты из колоды по очереди
    for card in cards_deck[0:deck_step]:
        if queue_counter % 2 == 0:
            player_cards.append(card)
            queue_counter += 1
        else:
            dealer_cards.append(card)
            queue_counter += 1

    # подсчитываем результаты. Диллер имеет 2 результата так как один показывает только одну карту
    dealer_real_result = get_result(dealer_cards, cards)
    dealer_result = get_result([dealer_cards[-1]], cards)
    player_result = get_result(player_cards, cards)

    if dealer_real_result > 21:
        bank += bet
        print_stats()
        print_winner('You win!')
        continue

    while player_result < 21:
        print_stats()

        # когда еще одна карта сдана повышаем счетчик шагов.
        deck_step += 1
        
        
        print('-------------------------------------------------')
        action = input(f'What are you going to do? {actions_list}\n')
        os.system('clear')
        
        if action == 'stand':
            break

        if action == 'split':
           # прячем карту 
           print('asdf')
           
        if action == 'double':
            bet *= 2

        player_cards = get_card(deck_step, cards_deck, player_cards)
        player_result = get_result(player_cards, cards)

    # Dealer game
    while dealer_real_result < 21:

        deck_step += 1

        choice = random.choice([True,False])
        if choice == True:
            dealer_cards = get_card(deck_step, cards_deck, dealer_cards)
            dealer_real_result = get_result(dealer_cards, cards)
        else:
            break

    print_stats()

    if dealer_real_result <= 21 and player_result <= 21:
        if dealer_real_result == player_result:
            if len(dealer_cards) == len(player_cards):
                winner = 'DRAW!'
            elif len(dealer_cards) < len(player_cards):
                winner = 'Dealer win!'
                bank -= bet
            else:
                winner = 'You win!'
                bank += bet
        elif dealer_real_result > player_result:
            winner = 'Dealer win!'
            bank -= bet
        else:
            winner = 'You win!'
            bank += bet
    elif dealer_real_result > 21 and player_result > 21:
        winner = 'DRAW!'
    elif dealer_real_result > 21:
        winner = 'You win!'
        bank += bet
    else:
        winner = 'Dealer win!'
        bank -= bet

    print_stats()

    print('-------------------------------------------------')
    cont = input(f'{winner.upper()} Do you want to continue: y/n ')
    if cont == 'y':
        continue
    else:
        break

print('End?')
