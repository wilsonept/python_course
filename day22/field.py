#!/usr/bin/python

from turtle import Turtle, Screen
from constants import *

class Field(Turtle):
    def __init__(self):
        super().__init__()
        self.draw_border(START_POINT, FIELD_LINE_WIDTH, FIELD_LINE_HEIGHT)
        self.draw_net(NET_DASH_LENGHT, FIELD_LINE_HEIGHT)
    
    def draw_border(self, start_point, field_line_width, field_line_height):
        self.hideturtle()
        self.color('gray')
        self.penup()
        self.goto(start_point)
        self.pendown()
        for _ in range(2):
            self.forward(field_line_width)
            self.left(90)
            self.forward(field_line_height)
            self.left(90)

    def draw_dash_line(self, net_dash_length, field_line_height):
        remain = field_line_height % net_dash_length
        while field_line_height != remain:
            if not self.isdown():
                self.pendown()
            else:
                self.penup()

            self.forward(net_dash_length)
            field_line_height -= net_dash_length

        if not self.isdown():
            self.pendown()
        else:
            self.penup()
        self.forward(remain)

    def draw_net(self, net_dash_length, field_line_height):
        self.color('white')
        self.penup()
        self.goto(0,self.ycor())
        self.left(90)
        self.pendown
        self.draw_dash_line(net_dash_length, field_line_height)