#!/bin/python

from leap_year import leap_or_not as is_leap

def days_in_month(year, month):
    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    if month == 2:
        if is_leap(year):
            return 29

    month -= 1
    return month_days[month]
