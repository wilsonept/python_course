#!/bin/python

from game_data import data
from art import logo, vs
from random import randint
from os import system

RANDOM_RANGE_LOWER = 0
RANDOM_RANGE_HIGHER = 49

def new_random(excluded):
    global RANDOM_RANGE_LOWER
    global RANDOM_RANGE_HIGHER
    result = randint(RANDOM_RANGE_LOWER,RANDOM_RANGE_HIGHER)
    if result == excluded:
        result = new_random(excluded)
    return result

def print_head(A,B,logo,vs):
    print(logo)
    print(f'Compare A: {A["name"]}, a {A["description"]} from {A["country"]}. [DEBUG] {A["follower_count"]}')
    print(vs)
    print(f'Against B: {B["name"]}, a {B["description"]} from {B["country"]}. [DEBUG] {B["follower_count"]}')

def get_winner(A,B):
    '''returns string "A" or "B"'''
    if A['follower_count'] > B['follower_count']:
        return 'A'
    else:
        return 'B'

def game():
    score = 0
    A_index = randint(RANDOM_RANGE_LOWER,RANDOM_RANGE_HIGHER) 
    A = data[A_index]
    B_index = new_random(A_index)
    B = data[B_index]

    game_is_over = False
    while not game_is_over:
        system('clear')
        print_head(A,B,logo,vs)
        if score > 0:
            print(f'Noice, keep going! Your score is "{score}"')

        winner = get_winner(A,B)
        guess = input('Who has more followers? "A" or "B": ').upper()
        if guess == winner:
            score += 1
            if guess == 'A':
                B_index = new_random(A_index)
                B = data[B_index]
            else:
                A_index = new_random(B_index)
                A = data[A_index]
                ## making swap
                C = A
                A = B
                B = C
        else:
            game_is_over = True
            print(f'You lose! Your score is "{score}"')
        

    resume = input('Do you want to play again? y/n ')
    if resume == 'y':
        game()

game()
