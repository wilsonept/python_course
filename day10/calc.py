#!/bin/python

import os

logo = '''
###########################
### CALC
###########################
'''

def add(a,b):
    a = float(a)
    b = float(b)
    result = a + b
    return result

def subtract(a,b):
    a = float(a)
    b = float(b)
    result = a - b
    return result

def multiply(a,b):
    a = float(a)
    b = float(b)
    result = a * b
    return result

def divide(a,b):
    a = float(a)
    b = float(b)
    result = a / b
    return result


print(logo)

resume = True
cont = ''
while resume:
    if cont != "y":
        a = input('Enter first number: ')

    action = input('What action do you want?\n+\n-\n*\n/\n')
    b = input('Enter second number: ')
    
    operations = {
        '+' : add,
        '-' : subtract,
        '*' : multiply,
        '/' : divide
    }

    function = operations[action]
    a = function(a,b)
    print(a)

    cont = input('Do you want to continue with current result? "y" or "n" ')
    if cont == "y":
        continue

    again = input('Do you want to start over? "y" or "n" ')
    if again == "y":
        resume = True
    else:
        resume = False
