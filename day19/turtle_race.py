#!/usr/bin/python3.9

from turtle import Turtle, Screen
from random import randint

is_race_on = False
screen = Screen()
screen.setup(width=600, height=400)

colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple']
turtles = []

starting_point = {}
starting_point['x'] = -270
starting_point['y'] = -125

for color in colors:
    tim = Turtle(shape='turtle')
    tim.color(color)
    tim.speed('fastest')
    tim.penup()
    tim.goto(starting_point['x'], starting_point['y'])

    starting_point['y'] += 50

    turtles.append(tim)

user_bet = screen.textinput(title="Make your bet", prompt="Which turtle will win the race? Enter a color: ")
print(user_bet)

if user_bet:
    is_race_on = True

while is_race_on:
    for turtle in turtles:
        distance = randint(0, 10)
        turtle.forward(distance)
        position = turtle.position()
        if position[0] >= 270:
            winner_color = turtle.pencolor()
            if user_bet == winner_color:
                print(f'You are pretty lycky bastart! {winner_color} win!')
                is_race_on = False
            else:
                print(f'Haha {winner_color} is the winner, better luck next time')
                is_race_on = False


screen.exitonclick()