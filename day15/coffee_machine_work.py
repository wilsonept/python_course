#!/usr/bin/python3.5
#!/bin/python

import time, random
from os import system

logo = '''
                     _                                __  __
                    (_)                              / _|/ _|
  _ __ _   _ ___ ___ _  __ _ _ __   ___     ___ ___ | |_| |_ ___
 | '__| | | / __/ __| |/ _` | '_ \ / _ \   / __/ _ \|  _|  _/ _ \\
 | |  | |_| \__ \__ \ | (_| | | | | (_) | | (_| (_) | | | ||  __/
 |_|   \__,_|___/___/_|\__,_|_| |_|\___/   \___\___/|_| |_| \___|
===================================================================
'''

recipes = []
latte_recipe = {
    'name' : 'latte',
    'price' : 30,
    'ingredients' : {
        'water' : 200,
        'milk' : 150,
        'coffee' : 24
    }
}
espresso_recipe = {
    'name' : 'espresso',
    'price' : 20,
    'ingredients' : {
        'water' : 100,
        'coffee' : 20
    }
}
recipes.append(latte_recipe)
recipes.append(espresso_recipe)

resources = {
    'water' : 190,
    'milk' : 300,
    'coffee' : 40
}

currency = {
    'wood' : 1,
    'copper' : 2,
    'silver' : 5,
    'gold' : 10
}


def get_access():
    password = input('Enter administrator password: ')
    if password == '1234':
        print('Access granted.')
        return True
    else:
        print('Access denied.')
        return False

def maintenance(resources):

    def m_report(*args):
        for resource in resources:
            print(resource + ': ' + str(resources[resource]))
        return True

    def m_resupply(*args):
        print('resupply')
        return True

    def m_quit(*args):
        print('quit')
        return False

    def m_help(*args):
        for command in commands:
            print(command)
        return True

    commands = {}
    commands["help"] = m_help
    commands["report"] = m_report
    commands["resupply"] = m_resupply
    commands["quit"] = m_quit

    print('======= Maintenance mode =======')
    m_help()
    should_continue = True
    while should_continue:
        command = input('Enter command: ')
        should_continue = commands[command](commands, resources)

# наливаем чашечку
def pour_cup(chosen_drink, resources):
    for ingredient in chosen_drink["ingredients"]:
        resources[ingredient] -= chosen_drink["ingredients"][ingredient]
        print('{ingredient} pouring...')
        time.sleep(random.randint(1,2))

    return resources

def give_change(currency, change):
    # TODO написать функцию которая будет возвращать монеты в зависимости от суммы
    print('')

def show_menu(recipes, additional_commands):
    ''' если все верно то возвращает индекс, если maintenance то -1'''
    system('clear')
    print(logo)
    counter = 0
    valid_answers = []
    valid_answers += additional_commands

    print('========== Menu ==========')
    for recipe in recipes:
        counter += 1
        valid_answers.append(str(counter))
        print(str(counter) + ") " + recipe['name'] + ": " + str(recipe['price']) + 'rub')

    user_choice = input('what would you like to drink? ')
    if not user_choice in valid_answers:
        print('ERROR: wrong answer, restarting...')
        time.sleep(1)
        show_menu(recipes, additional_commands)
    elif user_choice == additional_commands[0]:
        return -1
    chosen_drink_index = int(user_choice) - 1
    print('you chose: ' + recipes[chosen_drink_index]['name'])

    return chosen_drink_index

# проверяем ресурсы
def check_resources(chosen_drink, resources):
    operation_state = True
    for ingredient in chosen_drink['ingredients']:
        if resources[ingredient] < chosen_drink['ingredients'][ingredient]:
            operation_state = False
            print('{ingredient}: ERROR: not enough resource')
        else:
            print('{ingredient}: OK')

    return operation_state

def get_payment(currency, chosen_drink):
    print('You can use {currency} to pay your coffee')
    print('Your coffee costs: {chosen_drink["price"]}rub')
    payment_value = 0
    price_left = chosen_drink['price']
    while chosen_drink['price'] > payment_value:
        payment_coin = input('{price_left}rub left. What kind of coin do you want to use? ')
        # TODO проверить монеты, если не подходят, запросить еще раз
        payment_value += currency[payment_coin]
        time.sleep(1)
        price_left -= currency[payment_coin]
        if price_left <= 0:
           print('payment successfull')
           print('your change is {price_left * -1}')
        else:
            print('{price_left} left to pay your coffee')

    return True

def coffee_machine(resources, recipes, currency):

    additional_commands = ['maintenance']
    should_stay_in_menu = True
    while should_stay_in_menu:
        chosen_drink_index = show_menu(recipes, additional_commands)
        if chosen_drink_index == -1:
            if get_access() == True:
                maintenance(resources)
        else:
            # выбранный напиток <dict>
            chosen_drink = recipes[chosen_drink_index]
            operation_state = check_resources(chosen_drink, resources)
            if operation_state == True:
                should_stay_in_menu = False

    payment_succeded = get_payment(currency, chosen_drink)
    if payment_succeded:
        # TODO give_change()

        resources = pour_cup(chosen_drink, resources)
        print('Your coffee is done, Bon apetit!')

print(resources)

coffee_machine(resources, recipes, currency)

# код
#

