#!/bin/python

import time, random
from os import system

recipes = []
latte_recipe = {
    'name' : 'latte',
    'price' : 30,
    'ingredients' : {
        'water' : 200,
        'milk' : 150,
        'coffee' : 24
    }
}
espresso_recipe = {
    'name' : 'espresso',
    'price' : 20,
    'ingredients' : {
        'water' : 100,
        'coffee' : 20
    }
}
recipes.append(latte_recipe)
recipes.append(espresso_recipe)

resources = {
    'water' : 190,
    'milk' : 300,
    'coffee' : 40
}

currency = {
    'wood' : 1,
    'copper' : 2,
    'silver' : 5,
    'gold' : 10
}

# print(recipes)
# testing variable
#test_ingredients = recipes[1]['ingredients']
# если пользователь выбрал первый напиток
choice = '2'
# выбранный напиток <dict>
chosen_drink = recipes[int(choice) - 1]

# наливаем чашечку
def pour_cup(chosen_drink, resources):
    for ingredient in chosen_drink["ingredients"]:
        resources[ingredient] -= chosen_drink["ingredients"][ingredient]
        print(f"{ingredient} pouring...")
        time.sleep(random.randint(1,2))

    return resources

def give_change(currency, change):
    # TODO написать функцию которая будет возвращать монеты в зависимости от суммы
    print('')


# проверяем ресурсы
def check_resources(chosen_drink, resources):
    operation_state = True
    for ingredient in chosen_drink['ingredients']:
        if resources[ingredient] < chosen_drink['ingredients'][ingredient]:
            operation_state = False
            print(f'{ingredient}: ERROR: not enough resource')
        else:
            print(f'{ingredient}: OK')

    return operation_state

def get_payment(currency, chosen_drink):
    print(f'You can use {currency} to pay your coffee')
    print(f'Your coffee costs: {chosen_drink["price"]}rub')
    payment_value = 0
    price_left = chosen_drink['price']
    while chosen_drink['price'] > payment_value:
        payment_coin = input(f'{price_left}rub left. What kind of coin do you want to use? ')
        # TODO проверить монеты, если не подходят, запросить еще раз
        payment_value += currency[payment_coin]
        time.sleep(1)
        price_left -= currency[payment_coin]
        if price_left <= 0:
           print('payment successfull')
           print(f'your change is {price_left * -1}')
        else:
            print(f'{price_left} left to pay your coffee')

    return True


# код
operation_state = check_resources(chosen_drink, resources)
if operation_state == True:
    if get_payment(currency, chosen_drink):
        #give_change()
        resources = pour_cup(chosen_drink, resources)
        print('Your coffee is done, Bon apetit!')

print(resources)
