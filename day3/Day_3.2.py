#!/bin/python

weight = input("Enter weight in 'kg': ")
height = input("Enter height in 'm': ")

bmi = float(weight) / (float(height) ** 2)
bmi_r = round(bmi, 2)

if bmi <= 18.5:
    print(f'Your BMI is {bmi_r}, and you are underweight')
elif bmi <= 25:
    print(f'Your BMI is {bmi_r}, and you are normal weight')
elif bmi <= 30:
    print(f'Your BMI is {bmi_r}, and you are overweight')
elif bmi <= 35:
    print(f'Your BMI is {bmi_r}, and you are ovese')
else:
    print(f'Your BMI is {bmi_r}, and you are clinically obese.')

