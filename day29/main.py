#!/bin/python

from tkinter import Button, Tk, PhotoImage, Canvas, Label, Entry, DISABLED, messagebox
from random import choice
import pyperclip
import re
import json

# ------------------------------------- CONSTANTS -------------------------------------
FILE='pic/key_r2.png'
DB='db.json'

ASCII_TABLE = [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 125, 126]

WINDOW_WIDTH = 500
WINDOW_HEIGHT = 300

TITLE = "Key Chain"
FONT_NAME = "Courier"

DARKESTBLUE = "#1A1A2E"
DARKBLUE = "#16213E"
BLUE = "#0F3460"
GREEN = "#45e960"

# ------------------------------------- CHECKING DATA -------------------------------------
def get_inputdata(field_obj):
    input_data = field_obj.get()
    return input_data.strip()

def check_inputdata(*args):
    for item in args:
        if item =='':
            print('fill every single field')
            return False
        else:
            return True

# ------------------------------------- ADDING DATA -------------------------------------
def add_data():
    website = get_inputdata(field_web)
    username = get_inputdata(field_username)
    password = get_inputdata(field_pass)

    if check_inputdata(website, username, password) == True:
        new_data = {
            website: {
                "username": username,
                "password": password
            }
        }
        try:
            with open(DB, mode='r') as db_file:
                db_data = json.load(db_file)
                db_data.update(new_data)
        except (FileNotFoundError, json.decoder.JSONDecodeError) as e:
            db_data = new_data
        finally:
            with open(DB, mode='w') as db_file:
                json.dump(db_data, db_file, indent=4)
            field_web.delete(0, 'end')
            field_pass.delete(0, 'end')
    else:
        messagebox.showinfo(title='error', message="hey you, enter every single field")

# ------------------------------------- SEARCHING DATA -------------------------------------
def search_entry():
    field_pass.delete(0, 'end')
    website = get_inputdata(field_web)
    if check_inputdata(website):
        try:
            with open(DB, mode='r') as db_file:
                db_data = json.load(db_file)
        except (FileNotFoundError, json.decoder.JSONDecodeError) as e:
            messagebox.showinfo(title='No data in database', message="Your database is empty or it doesn't exist")
        else:
            # retrieve password from json file
            if website in db_data:
                password = db_data[website]['password']
                field_pass.insert(0, password)
                pyperclip.copy(password)
            else:
                messagebox.showinfo(title='Not Found', message="There are no entry for your query")
                
# ------------------------------------- PASSWORD GENERATION -------------------------------------
def generate_pass():
    password_len = 16
    ascii_table = ASCII_TABLE
    password = ""
    for _ in range(password_len):
        char = chr(choice(ascii_table))
        password += char
    return password

def new_pass():
    password = generate_pass()
    field_pass.delete(0, 'end')
    field_pass.insert(0, password)
    pyperclip.copy(password)

# ------------------------------------- WINDOW -------------------------------------
window = Tk()
window.resizable(0,0)
window.config(bg=DARKESTBLUE, padx=40, pady=20)
window.title(TITLE)
#window.grid()

# ------------------------------------- CANVAS FOR BACKGROUND AND TITLE -------------------------------------
image = PhotoImage(file=FILE)
canvas = Canvas(width=224, height=200, bg=DARKESTBLUE, highlightthickness=0)
canvas.create_image(112, 100, image=image)
title_text = canvas.create_text(112, 50, text=TITLE, font=(FONT_NAME, 26, 'bold'), fill=GREEN)
canvas.grid(row=0, column=1)

# ------------------------------------- website label and website field -------------------------------------
label_web = Label(text='Website : ', font=(FONT_NAME, 15, 'bold'), fg=BLUE, bg=DARKESTBLUE, anchor='e', width=11)
label_web.grid(row=1,column=0)
## website field
field_web = Entry(font=(FONT_NAME, 15, 'bold'), fg=GREEN, bg=DARKESTBLUE, width=22, highlightthickness=0, relief='ridge', insertbackground=GREEN, border=0)
field_web.focus()
field_web.grid(row=1,column=1, columnspan=2, sticky="w", pady=10)
## search web button
button_web = Button(text='sch', font=(FONT_NAME, 15, 'bold'), fg=GREEN, bg=DARKBLUE, width=7, height=1, highlightthickness=0, command=search_entry, relief='flat')
button_web.grid(row=1, column=2, sticky="w", pady=10, padx=10)

# ------------------------------------- username label and username field -------------------------------------
label_username = Label(text='Username : ', font=(FONT_NAME, 15, 'bold'), fg=BLUE, bg=DARKESTBLUE, anchor='e', width=11)
label_username.grid(row=2,column=0, pady=10)
## username field 
field_username = Entry(font=(FONT_NAME, 15, 'bold'), fg=GREEN, bg=DARKESTBLUE, width=30, highlightthickness=0, insertbackground=GREEN, border=0)
field_username.insert(0, 'wilsonept@gmail.com')
field_username.grid(row=2,column=1, columnspan=2, sticky="w", pady=10)

# ------------------------------------- password label and password field and password generator button -------------------------------------
label_pass = Label(text='Password : ', font=(FONT_NAME, 15, 'bold'), fg=BLUE, bg=DARKESTBLUE, anchor='e', width=11)
label_pass.grid(row=3,column=0, pady=10)
## password field
field_pass = Entry(font=(FONT_NAME, 15, 'bold'), fg=GREEN, bg=DARKESTBLUE, width=22, highlightthickness=0, insertbackground=GREEN, border=0)
field_pass.grid(row=3,column=1, sticky="w", pady=10)
field_pass.config()
## generate password button
button_pass = Button(text='gen', font=(FONT_NAME, 15, 'bold'), fg=GREEN, bg=DARKBLUE, width=7, height=1, highlightthickness=0, command=new_pass, relief='flat')
button_pass.grid(row=3, column=2, sticky="w", pady=10, padx=10)

# ------------------------------------- add button -------------------------------------
button_pass = Button(text='add', font=(FONT_NAME, 15, 'bold'), fg=DARKBLUE, bg=GREEN, width=30, height=1, highlightthickness=0, command=add_data, relief='flat')
button_pass.grid(row=4, column=1, columnspan=2, pady=10, sticky='w')

window.mainloop()