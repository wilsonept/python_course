#!/usr/bin/python
from turtle import Turtle
from time import sleep

START_POSITION = (0, 0)
SNAKE_LENGTH = 10
SNAKE_STEP = 20

UP = 90
RIGHT = 0
LEFT = 180
DOWN = 270

class SnakeBody():
    def __init__(self):
        self.length = SNAKE_LENGTH    
        self.step = SNAKE_STEP
        self.body = []
        for i in range(self.length):
            tim = Turtle(shape='square')
            tim.color('green')
            tim.penup()
            tim.setposition(START_POSITION)
            if i != 0:
                x_pos = tim.position()[0] - self.step * i
                tim.setposition(x_pos,0)
            else:
                self.head = tim
            self.body.append(tim)
            self.prev_heading = self.head.heading()
    
    def grow(self):
        tim = Turtle(shape='square')
        tim.color('green')
        tim.penup()
        tim.setposition(self.body[-1].position())
        self.body.append(tim)
        self.length += 1

    def move_forward(self):
        for i in range(self.length - 1, 0, -1):
            new_pos = self.body[i - 1].position()
            self.body[i].setposition(new_pos)
        self.head.forward(self.step)
        self.prev_heading = self.head.heading()

    def go_up(self):
        if self.head.heading() != DOWN and self.prev_heading != DOWN:
            self.head.setheading(UP)
    
    def go_right(self):
        if self.head.heading() != LEFT and self.prev_heading != LEFT:
            self.head.setheading(RIGHT)

    def go_left(self):
        if self.head.heading() != RIGHT and self.prev_heading != RIGHT:
            self.head.setheading(LEFT)

    def go_down(self):
        if self.head.heading() != UP and self.prev_heading != UP:
            self.head.setheading(DOWN)