#!/usr/bin/python

from field import Field
from turtle import Screen
from constants import *
from paddle import Paddle
from time import sleep
from ball import Ball
from scoreboard import Scoreboard

START_GAME_SPEED = 0.05

screen = Screen()
screen.setup(width=SCREEN_WIDTH, height=SCREEN_HEIGHT)
screen.bgcolor('black')
screen.title('Ping Pong the Game')
screen.tracer(0)

# OBJECT ASSIGNMENT
field = Field()
r_score = Scoreboard(RIGHT_SCORE_POS)
l_score = Scoreboard(LEFT_SCORE_POS)
r_paddle = Paddle(PADDLE_STEP, RIGHT_PADDLE_POS)
l_paddle = Paddle(PADDLE_STEP, LEFT_PADDLE_POS)
ball = Ball(BALL_SPEED)

# KEYBINDINGS
screen.listen()
screen.onkey(r_paddle.up, 'Up')
screen.onkey(r_paddle.down, 'Down')
screen.onkey(l_paddle.up, 'w')
screen.onkey(l_paddle.down, 's')
screen.onkey(l_paddle.up, 'W')
screen.onkey(l_paddle.down, 'S')

# GAME LOOP
start_game_speed = START_GAME_SPEED
game_is_on = True
while game_is_on:
    screen.update()
    ball.move()

    # UNBIND KEYS ON PADDLE AND WALL COLLISION
    ## UP KEY FOR RIGHT PADDLE
    if r_paddle.distance(r_paddle.xcor(), TOP_BOUNCE_POINT) < PADDLE_DIST_COLLISION:
        screen.onkey(None, 'Up')
    else:
        screen.onkey(r_paddle.up, 'Up')
    ## DOWN KEY FOR RIGHT PADDLE
    if r_paddle.distance(r_paddle.xcor(), BOT_BOUNCE_POINT) < PADDLE_DIST_COLLISION:
        screen.onkey(None, 'Down')
    else:
        screen.onkey(r_paddle.down, 'Down')
    ## W KEY FOR LEFT PADDLE
    if l_paddle.distance(l_paddle.xcor(), TOP_BOUNCE_POINT) < PADDLE_DIST_COLLISION:
        screen.onkey(None, 'W')
        screen.onkey(None, 'w')
    else:
        screen.onkey(l_paddle.up, 'W')
        screen.onkey(l_paddle.up, 'w')
    ## S KEY FOR LEFT PADDLE
    if l_paddle.distance(l_paddle.xcor(), BOT_BOUNCE_POINT) < PADDLE_DIST_COLLISION:
        screen.onkey(None, 'S')
        screen.onkey(None, 's')
    else:
        screen.onkey(l_paddle.down, 'S')
        screen.onkey(l_paddle.down, 's')

    # WALLS BOUNCE
    if ball.ycor() >= TOP_BOUNCE_POINT or ball.ycor() <= BOT_BOUNCE_POINT:
        ball.bounce_y()
        ball.squeze(wall=True, paddle=False)
        screen.update()
        ball.squeze(wall=False, paddle=False)
    
    # PADDLE BOUNCE
    if ball.xcor() == RIGHT_BOUNCE_POINT and ball.distance(r_paddle) < PADDLE_DIST_COLLISION or ball.xcor() == LEFT_BOUNCE_POINT and ball.distance(l_paddle) < PADDLE_DIST_COLLISION:
        ball.bounce_x()
        ball.squeze(wall=False, paddle=True)
        screen.update()
        ball.squeze(wall=False, paddle=False)
        # INCREASE GAME SPEED
        if start_game_speed > 0.01:
            start_game_speed -= 0.005


    # LEFT OR RIGHT COLLISION
    elif ball.xcor() >= RIGHT_CRIT_POINT:
        l_score.add()
        ball.restart()
        start_game_speed = START_GAME_SPEED
        sleep(1)

    elif ball.xcor() <= LEFT_CRIT_POINT:
        r_score.add()
        ball.restart()
        start_game_speed = START_GAME_SPEED
        sleep(1)

    sleep(start_game_speed)

screen.exitonclick()
