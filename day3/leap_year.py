#!/bin/python

def leap_or_not(year):

    div4 = year % 4
    if div4 == 0:
        div100 = year % 100
        if div100 == 0:
            div400 = year % 400
            if div400 == 0:
                return True 
            else:
                return True 
        else:
           return True 
    else:
       return False 

leap_or_not(2021)
