from tkinter import Entry, DISABLED, Tk, NORMAL, END, messagebox
import re

root = Tk()

class MyEntry(Entry):
    def __init__(self, text):
        super().__init__()
        self.width = 50
        self.insert(0, text)
        self.configure(state=DISABLED)

def on_click(event):
    my_entry.configure(state=NORMAL)
    my_entry.delete(0, END)

    # make the callback only work once
    my_entry.unbind('<Button-1>', on_click_id)

my_entry = MyEntry('hello dolly')
my_entry.pack()
###########################################################

DB='db.csv'
def check_entry(field_obj):
    input_data = get_inputdata(field_obj)
    with open(DB) as raw_data:
        data = raw_data.readlines()
        for item in data:
            match = re.match(input_data, item)
            if match != None:
                return True

on_click_id = my_entry.bind('<Button-1>', on_click)






check_entry()




root.mainloop()