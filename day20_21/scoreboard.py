#!/usr/bin/python

from turtle import Turtle

SCORE_POINTS = 10
FONT = ('Consolas', 12, 'normal')
ALIGN = 'Center'


class ScoreBoard(Turtle):
    def __init__(self):
        super().__init__()
        self.food_count = 0
        self.score = 0
        self.goto(0,280)
        self.color('white')
        self.hideturtle()
            
    def show_scoreboard(self):
        self.clear()
        self.write(f'Current Score: {self.score}', move=False, align=ALIGN, font=FONT)

    def add_score(self):
        self.score += SCORE_POINTS
        self.food_count += 1
    
    def game_over(self):
        self.penup()
        self.goto(0, 0)
        self.write(f'GAME OVER', move=True, align=ALIGN, font=FONT)