#!/usr/bin/python

from turtle import Turtle
from constants import *

class Scoreboard(Turtle):
    def __init__(self, x_pos):
        super().__init__()
        self.color('gray')
        self.penup()
        self.pensize(width=8)
        self.hideturtle()
        self.goto(x_pos, 0)
        self.score = 0
        self.write(self.score, font=('Consolas', 46, 'bold'), align='center')
    
    def add(self):
        self.score += 1
        self.clear()
        self.write(self.score, font=('Consolas', 46, 'bold'), align='center')