#!/bin/python

import os

names_n_bids = {}
names = []

resume = 'yes'

def find_highest_bidder(bidding_record):
    bid_highest = 0
    for key in bidding_record:
        bid_amount = bidding_record[key]
        if bid_amount > bid_highest:
            bid_highest = bid_amount
            key_highest = key

    return { key_highest : bid_highest }

def print_end(result):
    for key in result:
        name = key
        bid = result[key]

    print('The winner is {}, with the highest bid of {}!'.format(name,bid))

while resume == 'yes':
    name = input('What is your name?: ')
    bid = int(input("What's your bid?: $"))

    names_n_bids[name] = bid

    resume = input("Are there any other bidders? Type 'yes' or 'no': ")
    os.system('clear')


print_end(find_highest_bidder(names_n_bids))
