#!/usr/bin/python

from turtle import Turtle
from constants import *

class Paddle(Turtle):
    def __init__(self, paddle_step, paddle_pos):
        super().__init__()
        self.color('white')
        self.shape('square')
        self.shapesize(stretch_len=5, stretch_wid=0.8, outline=5)
        self.left(90)
        self.penup()
        self.goto(paddle_pos)
        self.paddle_step = paddle_step

    def up(self):
        self.forward(self.paddle_step)
    
    def down(self):
        self.back(self.paddle_step)