from tkinter import Tk, Canvas, Button, PhotoImage, messagebox
from tkinter.constants import FLAT, SUNKEN
import pandas
import random

BACKGROUND_COLOR = "#B1DDC6"
FONT_LANG = ('Arial', 40, 'italic')
FONT_WORD = ('Arial', 60, 'bold')
FONT_TIMER = ('Arial', 25, 'bold')

TIMEOUT = 3
TIMEOUT_MS = TIMEOUT * 1000

GAME_DATA_FILE = 'data/wordlist.csv'
GAME_PROGRESS_FILE = 'data/player_progress.csv'

# ---------------------------------- FUNCTIONS ----------------------------------
def start_timer(func):
    seconds = TIMEOUT
    count_down(seconds)
    global timer
    timer = window.after(TIMEOUT_MS, func)

def count_down(seconds):
    text = str(seconds)
    card.itemconfig(text_timer, text=text)
    global countdown
    countdown = window.after(1000, count_down, seconds - 1)

def reset_timer():
    global timer
    global countdown
    try:
        window.after_cancel(timer)
        window.after_cancel(countdown)
    except ValueError:
        pass

def remove_card(card_data):
    global data
    data.remove(card_data)

def update_progress_file():
    data_file = pandas.DataFrame.from_dict(data)
    data_file.to_csv(GAME_PROGRESS_FILE, index=False)

def confirm():
    remove_card(card_data)
    update_progress_file()
    get_card()

def get_card():
    reset_timer()
    start_timer(flip_card)
    global card_data
    if len(data) < 1:
        end_game()
    else:
        card_data = random.choice(data)
        card.itemconfig(text_word, text=card_data['en'], fill='black')
        card.itemconfig(text_lang, text='English', fill='black')
        card.itemconfig(card_image, image=front_image)

def flip_card():
    reset_timer()
    #start_timer(get_card)
    global card_data
    card.itemconfig(text_word, text=card_data['ru'], fill='white')
    card.itemconfig(text_lang, text='Русский', fill='white')
    card.itemconfig(text_timer, text='')
    card.itemconfig(card_image, image=back_image)

def load_game_data(filepath):
    data_file = pandas.read_csv(filepath_or_buffer=filepath)
    return data_file

def end_game():
    answer = messagebox.askyesno(title='Congratulations!', message='Congratulations! You have learned all of the words, do you want to start over?')
    if answer:
        data_file = load_game_data(GAME_DATA_FILE)
        data_file.to_csv(path_or_buf=GAME_PROGRESS_FILE, index=False)
        return data_file
    else:
        window.quit()

# ---------------------------------- WINDOW ----------------------------------
window = Tk()
window.resizable(0,0)
window.config(bg=BACKGROUND_COLOR, padx=50, pady=50)

# ---------------------------------- CARD ----------------------------------
front_image = PhotoImage(file='images/card_front.png')
back_image = PhotoImage(file='images/card_back.png')
card = Canvas(width=800, height=526, highlightthickness=0, bg=BACKGROUND_COLOR)
card_image = card.create_image(400, 263, image=front_image)
card.grid(row=0, column=0, columnspan=2)
# ------------- TEXT -------------
text_lang = card.create_text(400, 150, font=FONT_LANG, text='language')
text_word = card.create_text(400, 263, font=FONT_WORD, text='word')
text_timer = card.create_text(400, 400, font=FONT_TIMER, text='', fill='gray')
# ---------------------------------- BUTTONS ----------------------------------
# ------------- WRONG -------------
button_wrong_image = PhotoImage(file='images/wrong.png')
button_wrong = Button(image=button_wrong_image, highlightthickness=0, border=0, relief=SUNKEN, command=get_card, background=BACKGROUND_COLOR, bg=BACKGROUND_COLOR)
button_wrong.grid(row=1, column=0)
# ------------- RIGHT -------------
button_right_image = PhotoImage(file='images/right.png')
button_right = Button(image=button_right_image, highlightthickness=0, border=0, relief=SUNKEN, command=confirm, bg=BACKGROUND_COLOR)
button_right.grid(row=1, column=1)


# -------------------------------------------------------------------------------
# ---------------------------------- MAIN LOOP ----------------------------------
# -------------------------------------------------------------------------------
# ------------- GLOBALS -------------
card_data = {}
countdown = None
timer = None

# ------------- PREPARE PLAYER GAME PROGRESS -------------
try:
    data_file = load_game_data(GAME_PROGRESS_FILE)
except FileNotFoundError:
    data_file = load_game_data(GAME_DATA_FILE)
    data_file.to_csv(GAME_PROGRESS_FILE, index=False)
except pandas.errors.EmptyDataError:
    data_file = end_game()

data = data_file.to_dict(orient='records')


# ------------- START GAME -------------
get_card()


window.mainloop()