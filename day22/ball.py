#!/usr/bin/python

from turtle import Turtle
from constants import *
from random import randint

class Ball(Turtle):
    def __init__(self, ball_speed):
        super().__init__()
        self.shape('circle')
        self.color('white')
        self.penup()
        self.goto(0, randint(0, 100))
        self.speed = ball_speed
        self.x_multipiler = 1
        self.y_multipiler = 1

    def move(self):
        new_x = self.xcor() + self.speed * self.x_multipiler
        new_y = self.ycor() + self.speed * self.y_multipiler
        self.goto(new_x,new_y)

    def bounce_y(self):
        self.y_multipiler *= -1

    def bounce_x(self):
        self.x_multipiler *= -1

    def squeze(self, wall, paddle):
        if paddle:
            self.shapesize(stretch_len=0.5, stretch_wid=1.5)
        elif wall:
            self.shapesize(stretch_len=1.5, stretch_wid=0.5)
        else:
            self.shapesize(stretch_len=1, stretch_wid=1)
    
    def restart(self):
        self.goto(0, randint(0, 100))
        self.x_multipiler *= -1
        self.y_multipiler *= -1
