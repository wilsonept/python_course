#!/usr/bin/python

from tkinter import Tk, Canvas, PhotoImage, Label, Button
from time import strftime, gmtime
import os

os.chdir('C:\\!git\\Python\\day28')

# CONSTANTS ############################################
PINK = '#E2979C'
RED = '#E7305B'
GREEN = '#9BDEAC'
YELLOW = '#F7F5DD'
FONT_NAME = 'Courier'
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
CHECK_MARK = '✔'

# GLOBAL ###############################################
reps = 0
mytimer = None

# FUNCTIONS ############################################
def start_timer():
    global reps
    reps += 1
    window.attributes("-topmost", False)
    if reps % 8 == 0:
        label_timer.config(text='Break', fg=RED)
        seconds = LONG_BREAK_MIN * 60
    elif reps % 2 == 0:
        label_timer.config(text='Break', fg=PINK)
        seconds = SHORT_BREAK_MIN * 60
    else:
        label_timer.config(text='Work', fg=GREEN)
        seconds = WORK_MIN * 60
    count_down(seconds)

def count_down(seconds):
    new_text = strftime("%M:%S", gmtime(seconds))
    canvas.itemconfig(timer_text, text=new_text)
    if seconds > 0:
        global mytimer
        mytimer = window.after(1000, count_down, seconds - 1)
    else:
        start_timer()
        if reps % 2 == 0:
            label_checkmark.config(text=label_checkmark.cget('text') + CHECK_MARK)
            window.attributes("-topmost", True)

def reset_timer():
    global reps
    reps = 0
    window.after_cancel(mytimer)
    label_checkmark.config(text="")
    canvas.itemconfig(timer_text, text='00:00')
    label_timer.config(text='Timer')

# WINDOW ###############################################
window = Tk()
window.title('Pomodoro')
window.config(background=YELLOW, padx=100, pady=50)

# CANVAS FOR BACKGROUND AND TIMER ######################
image = PhotoImage(file='tomato.png')
canvas = Canvas(width=200, height=224, bg=YELLOW, highlightthickness=0)
canvas.create_image(100, 112, image=image)
timer_text = canvas.create_text(100, 130, text='00:00', font=(FONT_NAME, 30, 'bold'), fill='white')
canvas.grid(row=1, column=1)

# MAIN LABEL ###########################################
label_timer = Label(text='Timer', fg=GREEN, font=(FONT_NAME, 40, 'italic'), bg=YELLOW)
label_timer.grid(row=0, column=1)

# BUTTONS ##############################################
button_start = Button(text='Start', command=start_timer)
button_start.grid(row=2, column=0)

button_reset = Button(text='Reset', command=reset_timer)
button_reset.grid(row=2, column=2)

# CHECKMARK LABEL ######################################
label_checkmark = Label(fg=GREEN, font=(FONT_NAME, 15), bg=YELLOW)
label_checkmark.grid(row=3, column=1)


window.mainloop()