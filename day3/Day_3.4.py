#!/bin/python

print("Welcome ot Python Pizza Deliveries!\n")

menu = 'Small Pizza: $15','Medium Pizza: $20','Large Pizza: $25','','Pepperoni for Small Pizza: +$2','Pepperoni for Medium or Large Pizza: +$3','','Extra cheese for any size of pizza: + $1'
for item in menu:
    print(item)

size = input("What size pizza do you want? S, M, L: ")
if size == "S":
    bill = 15
elif size == "M":
    bill = 20
elif size == "L":
    bill = 25
else:
    print("Wrong Size, use S, M or L words!")
    exit()

add_pepperoni = input("Do you want pepperoni? Y, N: ")
if add_pepperoni == "Y":
    if size == "S":
        bill += 2
    else:
        bill += 3

extra_cheese = input("Do you want extra cheese? Y, N: ")
if extra_cheese == "Y":
    bill += 1

print(f"Your total bill is {bill}$")

