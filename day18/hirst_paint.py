#!/bin/python

from turtle import Turtle, Screen, colormode, circle
from random import choice, randint
from math import pi
from os import getcwd, chdir
import colorgram

chdir('C:\\Users\\wilso\\Downloads')
colors = colorgram.extract('61RQCX9SJKL.jpg', 19)
tuple_colors = []
for color in colors:
    r = color.rgb.r
    g = color.rgb.g
    b = color.rgb.b
    tuple_colors.append((r,g,b))

rgb_colors = tuple_colors[4:]
print(rgb_colors)

def draw_dots(turtle, dots_count, pensize, space_len, rgb_colors):
    while dots_count != 0:
        turtle.forward(space_len)
        turtle.dot(pensize, choice(rgb_colors))
        dots_count -= 1
    turtle.forward(space_len)
    turtle.setpos(-300,-300)

def new_line(turtle,space_len, linenumber):
    turtle.left(90)
    turtle.forward(space_len * linenumber)
    turtle.right(90)

t = Turtle()
t.hideturtle()
t.penup()
t.speed('fastest')
t.setpos(-300,-300)
colormode(255)
linenumber = 10
for number in range(1,linenumber + 1):
    draw_dots(t, 10, 20, 50, rgb_colors)
    new_line(t, 50, number)

screen = Screen()
