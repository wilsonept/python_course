#!/bin/python

import random
import blueprint

guesture = blueprint.rock, blueprint.paper, blueprint.scissors

print('What do you choose?')
choice = input('Type:\n    0 for Rock\n    1 for Paper\n    2 for Scissors\n')

if not choice.isdigit() or int(choice) > 2:
    print('Incorrect input, try \n    0 for Rock\n    1 for Paper\n    2 for Scissors')
    exit()

choice = int(choice)
comp_choice = int(random.choice([0,1,2]))


print(guesture[int(choice)])
print(guesture[comp_choice])

if choice == 0 and comp_choice == 1:
    print('YOU LOOSE')
elif choice == 0 and comp_choice == 2:
    print('YOU WIN')
elif choice == 1 and comp_choice == 2:
    print('YOU LOOSE')
elif choice == 1 and comp_choice == 0:
    print('YOU WIN')
elif choice == 2 and comp_choice == 0:
    print('YOU LOOSE')
elif choice == 2 and comp_choice == 1:
    print('YOU WIN')
else:
    print('DRAW')


