#!/usr/bin/python

import os
from turtle import Turtle, Screen, bye
import pandas

os.chdir('C:\\!git\\Python\\day25\\us-states-game-start')

class Typer(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.penup()

class InputBox(Turtle):
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.penup()


typer = Typer()

screen = Screen()
screen.setup(width=725, height=491)
screen.bgpic("blank_states_img.gif")
screen.title('Remmember Everything, USA edition')
choice = screen.textinput('Welcome to the Game', 'Enter the name of the state"')
if choice == None:
    game_is_on = False
else:
    choice = choice.lower().capitalize()

data = pandas.read_csv('50_states.csv')
states = data['state'].to_list()
chosen_states = []

correct = 0
game_is_on = True
while game_is_on:
    state_data = data[data['state'] == choice]
    if choice in states:
        states.remove(choice)
        typer.goto(state_data['x'].item(), state_data['y'].item())
        typer.write(choice, align='center', font=('Consolas', 12, 'normal'))
        correct += 1
    
    choice = screen.textinput(f'{correct}/50 States Correct', "What's another state name?" )
    if correct == 50 or choice == None:
        game_is_on = False
    else:
        choice = choice.lower().capitalize()

states_to_learn = {
    'StatesToLearn': states
}
new_csv = pandas.DataFrame(states_to_learn)
new_csv.to_csv('States_To_Learn.csv')

screen.exitonclick()