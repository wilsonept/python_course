#!/usr/bin/python

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600

from snake import SnakeBody
from food import Food
from scoreboard import ScoreBoard
from turtle import Screen
from time import sleep

screen = Screen()
screen.setup(width=SCREEN_WIDTH, height=SCREEN_HEIGHT)
screen.bgcolor("black")
screen.title("My Snake Game")
screen.tracer(0)

snake = SnakeBody()
food = Food()
scoreboard = ScoreBoard()
print(food.position())
screen.listen()
screen.onkeypress(key='Up', fun=snake.go_up)
screen.onkey(key='Right', fun=snake.go_right)
screen.onkey(key='Left', fun=snake.go_left)
screen.onkey(key='Down', fun=snake.go_down)
screen.onkey(key='w', fun=snake.move_forward)

game_is_on = True
# Walking through the screen
while game_is_on:
    if snake.head.xcor() > 299:
        snake.head.goto(-300,snake.head.ycor())
    elif snake.head.xcor() < -299:
        snake.head.goto(300,snake.head.ycor())
    elif snake.head.ycor() > 299:
        snake.head.goto(snake.head.xcor(), -300)
    elif snake.head.ycor() < -299:
        snake.head.goto(snake.head.xcor(), 300)

    scoreboard.show_scoreboard()
    food.detect_snake(snake, scoreboard)
    snake.move_forward()

    # Detect collision with the tail
    for segment in snake.body[1:]:
        if snake.head.distance(segment) < 10:
            game_is_on = False

    screen.update()
    sleep(0.1)

scoreboard.game_over()
screen.update()

screen.exitonclick()