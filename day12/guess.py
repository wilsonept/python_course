#!/bin/python

import random, os

EASY_LEVEL_TURNS = 10
HARD_LEVEL_TURNS = 5

logo = '''
============================================================
____ _  _ ____ ____ ____ _ _  _ ____    ____ ____ _  _ ____
| __ |  | |___ [__  [__  | |\ | | __    | __ |__| |\/| |___
|__] |__| |___ ___] ___] | | \| |__]    |__] |  | |  | |___

============================================================
Welcome to the Number Guessing Game!
I'm thinking of a number between 1 and 100.
'''
loser = '''
============================================================
                  _    ____ ____ ____ ____
                  |    |  | [__  |___ |__/
                  |___ |__| ___] |___ |  \\

============================================================
'''
winner = '''
============================================================
                _ _ _ _ _  _ _  _ ____ ____
                | | | | |\ | |\ | |___ |__/
                |_|_| | | \| | \| |___ |  \\

============================================================
'''

def get_turns():
    '''возвращает допустимое количество попыток'''
    global EASY_LEVEL_TURNS
    global HARD_LEVEL_TURNS
    difficulty = check_answer(input("Choose a difficulty. Type 'easy' or 'hard': "), 0)
    if difficulty == 'easy':
        return EASY_LEVEL_TURNS
    else:
        return HARD_LEVEL_TURNS

def make_guess():
    '''возвращает проверенный результат guess'''
    guess = check_answer(input('Make a guess: '), 1)
    return guess

def check_guess(guess, secret_number):
    '''возвращает результат игры, проиграл: "1", выиграл: "0"'''
    if secret_number == guess:
        print(winner)
        return 0
    elif secret_number < guess:
        print('Too high')
    else:
        print('Too low')
    return 1

def check_answer(answer, a_type):
    '''возвращает проверенный ответ для guess используем "a_type=1", для difficulty используем "a_type=0"'''
    if a_type == 1:
        if answer.isdigit():
            answer = int(answer)
            if answer < 1 or answer > 100:
                answer = int(check_answer(input('Type again, digit number from 1 to 100 to continue: '), 1))
        elif not answer.isdigit():
            answer = check_answer(input('Type again, digit number from 1 to 100 to continue: '), 1)
    elif a_type == 0:
        if answer != 'easy' and answer != 'hard':
            answer = check_answer(input("Type again, 'easy' or 'hard': "), 0)
    return answer

def play_game():
    # Получить рандомное число от 1 до 100
    secret_number = random.randint(1,100)
    # очистить вывод
    os.system('clear')
    print(f'[DEBUG] : {secret_number}')
    # Вывести лого
    print(logo)
    # попросить выбрать сложность
    turns = get_turns()
    print(f'You have {turns} attempts remainig to guess the number.')

    game_is_over = False
    while not game_is_over:
        # попросить выбрать число
        guess = make_guess()
        # проверить больше число или меньше
        result = check_guess(guess, secret_number)
        # уменьшаем количество оставшихся попыток
        turns -= 1
        print(f'You have {turns} attempts remainig to guess the number.')

        if result == 0:
            game_is_over = True
        elif turns < 1: 
            print(loser)
            game_is_over = True

    repeat = input('Do you want to repeat? y/n ')
    if repeat == 'y':
        play_game()


# Играем в нашу игру
play_game()
