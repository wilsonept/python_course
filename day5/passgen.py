#!/bin/python

import random

letters = list(range(65,91)) + list(range(97,123))
symbols = list(range(33,48)) + list(range(58,65)) + list(range(91,97)) + list(range(123,127))
numbers = list(range(48,58))

num_letters = int(input('How many letters would you like? '))
num_symbols = int(input('How many symbols would you like? '))
num_numbers = int(input('How many numbers would you like? '))

chosen_letters = []
chosen_symbols = []
chosen_numbers = []
password = []

for i in range(0, num_letters):
    chosen_letters.append(random.choice(letters))

for i in range(0, num_symbols):
    chosen_symbols.append(random.choice(symbols))

for i in range(0, num_numbers):
    chosen_numbers.append(random.choice(numbers))

chosen_ascii = chosen_letters + chosen_symbols + chosen_numbers

for i in range(0, len(chosen_ascii)):
    char = random.choice(chosen_ascii)
    chosen_ascii.remove(char)
    password.append(chr(char))

print("".join(password))
