#!/bin/python

days_in_year = 365
weeks_in_year = 52
months_in_year = 12
total_years = 90

total_days = total_years * days_in_year
total_weeks = total_years * weeks_in_year
total_months = total_years * months_in_year

age = input("Hello, What is your current age? ")
age_days = int(age) * days_in_year
age_weeks = int(age) * weeks_in_year
age_months = int(age) * months_in_year

dif_days = total_days - age_days
dif_weeks = total_weeks - age_weeks
dif_months = total_months - age_months

message = f"You have {dif_days} days, {dif_weeks} weeks, and {dif_months} monts left if you make it till 90"
print(message)
