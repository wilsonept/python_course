#!/usr/bin/python

import tkinter
import time
import os

os.chdir('C:\\!git\\Python\\day28')
print(os.getcwd())

BG_COLOR = '#faf7c6'
BG_PIC = 'tomato.png'

FONT = ('Futura', 28)

PADX = 10
PADY = 10
PIC_WIDTH = 200
PIC_HEIGHT = 223
WINDOW_WIDTH = PIC_WIDTH + PADX * 2
WINDOW_HEIGHT = PIC_HEIGHT + PADY * 2
WORK_TIME = 10
REST_TIME = 3

# Окно с фоновым цветом
window = tkinter.Tk()
window.geometry(f'{WINDOW_WIDTH}x{WINDOW_HEIGHT}')
window.resizable(0, 0)
window.config(bg=BG_COLOR, padx=PADX, pady=PADY)

# объект с фоновым изображением и цветом
image = tkinter.Image(imgtype='photo', file=BG_PIC)
background_image = tkinter.Label(image=image, background=BG_COLOR)
background_image.place(x=0, y=0)

# Заголовок
label = tkinter.Label(text='Work', font=FONT, background=BG_COLOR)
label.grid(column=1, row=0)

# Таймер
class Timer(tkinter.Label):
    def __init__(self):
        super().__init__()
        self.init_working_timer()
        self.working_counter = 0
        self.timer_text = time.strftime("%M:%S", time.gmtime(self.timer_counter))
        self.config(text=self.timer_text, font=FONT)
        self.grid(column=1, row=1)

    def decrease_timer(self):
        self.timer_counter -= 1
        self.timer_text = time.strftime("%M:%S", time.gmtime(self.timer_counter))
        self.config(text=self.timer_text, font=FONT)

    def init_working_timer(self):
        self.timer_counter = WORK_TIME
    
    def init_resting_timer(self):
        if self.working_counter == 3:
            self.timer_counter = WORK_TIME
        else:
            self.timer_counter = REST_TIME

should_reset = False
def start_timer():
    for i in range(4):
        print('***')
        print(should_reset)
        if should_reset == False:
            timer.init_working_timer()
            label.config(text='Work')
            while timer.timer_counter != 0 and should_reset == False:
                print('   ###')
                print(should_reset)
                timer.decrease_timer()
                window.update()
                time.sleep(1)
            label.config(text='Rest')
            timer.init_resting_timer()
            while timer.timer_counter != 0 and not should_reset == False:
                print('   ***')
                print(should_reset)
                timer.decrease_timer()
                window.update()
                time.sleep(1)
    
    if should_reset == False:
        print('start again')
        print(should_reset)
        start_timer()
    else:
        return

def reset_timer():
    global should_reset
    should_reset = True
    timer.__init__()

timer = Timer()

# Кнопка старта
start_button = tkinter.Button(text='start', command=start_timer)
start_button.grid(column=0, row=2)

# Кнопка сброса
reset_button = tkinter.Button(text='reset', command=reset_timer)
reset_button.grid(column=2, row=2)



window.mainloop()