#!/bin/python

print('Welcome to the tip calculator.')
total_bill_str = input('What was the total bill? \n$')
total_bill_float = float(total_bill_str)

tip_str = input('What percentage tip would you like to give? \n')
tip_int = int(tip_str)

grand_total = (tip_int / 100 + 1) * total_bill_float
print(f"GrandTotal: {grand_total}")

friend_count_str = input('How many people to split the bill? \n')
friend_count_int = int(friend_count_str)
print(f"Friend_count_int: {friend_count_int}")

result = "{:.2f}".format(round(grand_total / friend_count_int, 2))

print(f"Each person should pay: ${result}")

