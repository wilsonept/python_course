#!/usr/bin/python

import tkinter

FONT = ('Consolas', 12, 'bold')
PADDING = 10

window = tkinter.Tk()
window.title('Mile to Km Converter')
window.minsize(width=300, height=100)
window.config(padx=PADDING, pady=PADDING)


entry = tkinter.Entry(width=15)
entry.insert(tkinter.END, string='0')
entry.grid(column=1, row=0)

label_miles = tkinter.Label(text='Miles', font=FONT, justify='left', anchor='e', width=5)
label_miles.config(padx=PADDING, pady=PADDING)
label_miles.grid(column=2, row=0)

label_equality = tkinter.Label(text='is equal to', font=FONT, justify='right', anchor='e', width=10)
label_equality.config(padx=PADDING, pady=PADDING)
label_equality.grid(column=0, row=1)

label_km = tkinter.Label(text='Km', font=FONT, justify='left', anchor='e', width=5)
label_km.config(padx=PADDING, pady=PADDING)
label_km.grid(column=2, row=1)

label_value = tkinter.Label(text='0', font=FONT)
label_value.config(padx=PADDING, pady=PADDING)
label_value.grid(column=1, row=1)

def click():
    text = entry.get()
    km = float(text) * 1.609
    text = str(km)
    label_value.config(text=text)

button = tkinter.Button(text='calculate', command=click)
button.config(padx=PADDING, pady=5)
button.grid(column=1, row=2)

window.mainloop()