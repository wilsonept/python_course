#!/usr/bin/python3.9

from turtle import Turtle, Screen

def move_up():
    tim.setheading(90)
    tim.forward(10)

def move_left():
    tim.setheading(180)
    tim.forward(10)

def move_right():
    tim.setheading(0)
    tim.forward(10)

def move_down():
    tim.setheading(270)
    tim.forward(10)

def move_forward():
    tim.forward(10)

def move_backward():
    tim.backward(10)

def turn_right():
    tim.right(10)

def turn_left():
    tim.left(10)

def reset_screen():
    screen.reset()

tim = Turtle()
screen = Screen()
screen.setup(width=600, height=400)

screen.listen()
screen.onkey(key='Up', fun=move_up)
screen.onkey(key='Left', fun=move_left)
screen.onkey(key='Right', fun=move_right)
screen.onkey(key='Down', fun=move_down)

screen.onkey(key='w', fun=move_forward)
screen.onkey(key='s', fun=move_backward)
screen.onkey(key='a', fun=turn_left)
screen.onkey(key='d', fun=turn_right)

screen.onkey(key='c', fun=reset_screen)


screen.exitonclick()