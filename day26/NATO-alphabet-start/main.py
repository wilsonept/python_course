import os
import pandas

os.chdir('C:\\!git\\Python\\day26\\NATO-alphabet-start')

# Keyword Method with iterrows()
# {new_key:new_value for (index, row) in df.iterrows()}

#TODO 1. Create a dictionary in this format:
#{"A": "Alfa", "B": "Bravo"}

#TODO 2. Create a list of the phonetic code words from a word that the user inputs.


data = pandas.read_csv('nato_phonetic_alphabet.csv')
letters = {row.letter:row.code for (index, row) in data.iterrows()}
print(letters)

word = (input('Enter a word: ')).upper()

result = [letters[letter] for letter in word]
print(result)
    

